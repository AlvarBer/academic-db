create table Contracts(Ref  VARCHAR(10) PRIMARY KEY,
  Organization VARCHAR(100),
  ContDate DATE,
  NumRoutes NUMBER(2,0));
                       
create table Routes(Ref VARCHAR(10) REFERENCES Contracts ON DELETE CASCADE,
  Origin     	  VARCHAR(50),
  Destination    VARCHAR(50),
  Vehicle        VARCHAR(20),
  PRIMARY KEY (Ref, Origin, Destination));
  
  -- 2 a)
  
declare

begin
 
end

