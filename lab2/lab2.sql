-- Exercise 1)
-- 1.1)
select Book.Title, Book.A�o, Author.Name from Book natural join Author natural join Author_book
order by Book.A�o;

-- 1.2)
select Title from Book where A�o < '2000';

-- 1.3)
select distinct Name from Client natural join Orders natural join Books_order;

-- 1.4)
select Name from Client natural join Orders natural join Books_order 
where Books_order.ISBN='4554672899910';

-- 1.5)
select distinct Client.Name, Book.Title from Client natural join Orders 
natural join Books_order natural join Book 
where Client.Name like '%Jo%';

-- 1.6)
select distinct Client.Name from Client 
natural join Orders natural join Books_order natural join Book 
where Book.PurchasePrice > 10;

-- 1.7)
select name, dateorder, count(*) from client natural join orders 
group by idclient,dateorder, name
having count(*) > 1

-- 1.8)
select name, dateorder from client natural join orders
where dateexped > sysdate

-- 1.9)
select distinct name from client natural join Orders 
natural join Books_order natural join Book
where Book.PurchasePrice < 10