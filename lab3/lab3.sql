drop table Test;
create table Test (id number(2), name varchar(10));

insert into Test values(1, 'Fred');
select * from Test;

commit

insert into Test values(2, 'Barney');
insert into Test values(3, 'Wilma');
rollback;
insert into Test values(4, 'Betty');
commit;
insert into Test values(5, 'Sally');
rollback;
select * from Test;
delete from Test;
rollback;
commit;

-- Exercise 2
delete from Test;
commit;
set transaction read only name 'my_transaction';
insert into Test values(1, 'Fred');

commit;
set transaction read write;
insert into Test values(2, 'Barney');
commit;
select * from Test;

-- Exercise 3
-- a)
delete from Test;
commit;
insert into Test values(1, 'Fred');
savepoint my_sp1;
insert into Test values(2, 'Barney');
savepoint my_sp2;
insert into Test values(3, 'Wilma');
rollback to my_sp1;
commit;
select * from Test;

-- Exercise 4
drop table Test;
select object_name as 